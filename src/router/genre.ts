import { Router } from "express";
import { authenticateJWT } from "../middleware/auth";
import { genreModule } from "../module/Genre";
import { createGenreValidation, validate } from "./validation";

export const GenreRoute = Router();


GenreRoute.get('/list',genreModule.list);
GenreRoute.post('/create',authenticateJWT,createGenreValidation(),validate,genreModule.create);
