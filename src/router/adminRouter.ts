import { Router } from "express";
import { userModule } from "../module/User";
import { createUserValidation,loginValidation,validate } from "./validation";
export const AdminRoute = Router();


AdminRoute.post('/create',createUserValidation(),validate, userModule.create);
AdminRoute.post('/login',loginValidation(),validate, userModule.login);






