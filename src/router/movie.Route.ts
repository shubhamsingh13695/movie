import { Router } from "express";
import { authenticateJWT } from "../middleware/auth";
import { movieModule } from "../module/Movie";
import { createMovie, updateMovie, validate } from "./validation";

export const MovieRoute = Router();

MovieRoute.post('/create',authenticateJWT,createMovie(),validate,movieModule.create);
MovieRoute.get('/show/:id',authenticateJWT,movieModule.getMovie);
MovieRoute.post('/update/:id',authenticateJWT,updateMovie(),validate,movieModule.update);
MovieRoute.delete('/delete/:id',authenticateJWT,movieModule.deleteMovie);

MovieRoute.get('/list',movieModule.list);

