import { Router } from "express";
import { AdminRoute } from "./adminRouter";
import { GenreRoute } from "./genre";
import { MovieRoute } from "./movie.Route";

export const router = Router();

router.use('/admin',AdminRoute);
router.use('/movie',MovieRoute);
router.use('/genre',GenreRoute);
