import { body, validationResult } from "express-validator";
import { Request, Response,NextFunction } from "express";

export const createUserValidation = () => [
    body('name').exists().withMessage("Name is requred"),
    body('email').exists().withMessage("Email is requred").isEmail().withMessage("Please Enter Valid Email"),
    body('password').exists().withMessage("Password is required").isLength({ min: 5 }).withMessage("Password length sholud be more than 4")
]

export const loginValidation = () => [
  body('email').exists().withMessage("Email is requred").isEmail().withMessage("Please Enter Valid Email"),
  body('password').exists().withMessage("Password is required").isLength({ min: 5 }).withMessage("Password length sholud be more than 4")
]

export const createMovie = () => [
  body('name').exists().withMessage("Movie Name is requred"),
  body('popularity').exists().withMessage("Popularity is requred").isFloat({min:0,max:100}).withMessage("Please enter valid range"),
  body('director').exists().withMessage("Director Name is requred"),
  body('imdb_score').exists().withMessage("Imdb rating is requred").isFloat({min:0,max:10}).withMessage("Please enter valid range"),
  body('genre').exists().withMessage("Genre is requred"),
]

export const updateMovie = () => [
  body('popularity').exists().withMessage("Popularity is requred").isFloat({min:0,max:100}).withMessage("Please enter valid range"),
  body('director').exists().withMessage("Director Name is requred"),
  body('imdb_score').exists().withMessage("Imdb rating is requred").isFloat({min:0,max:10}).withMessage("Please enter valid range"),
  body('genre').exists().withMessage("Genre is requred"),
]


export const createGenreValidation = () => [
  body('name').exists().withMessage("Genre Name is requred"),
]

export const validate = (req:Request,res:Response,next:NextFunction) =>{
  const errors = validationResult(req);
  console.log(errors);
    if (errors.isEmpty()) {
      return next()
    }
    const extractedErrors:any[] = []
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))
  
    return res.status(422).json({
      validationError: extractedErrors,
    })
}




