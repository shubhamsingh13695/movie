
import { mongoose, prop, getModelForClass } from '@typegoose/typegoose';
export class Genre{
    
    @prop({type:mongoose.Schema.Types.String})
    name!: string;
}

export const GenreModel = getModelForClass(Genre, { schemaOptions: { timestamps: true } });