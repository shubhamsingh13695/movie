
import { mongoose, prop, getModelForClass } from '@typegoose/typegoose';
export class User{
    
    @prop({type:mongoose.Schema.Types.String})
    name!: string;

    @prop({type:mongoose.Schema.Types.String})
    email!:string;

    @prop({type:mongoose.Schema.Types.String})
    password!:string;

    @prop({type:mongoose.Schema.Types.Boolean})
    status!:boolean;

}

export const UserModel = getModelForClass(User, { schemaOptions: { timestamps: true } });