import { mongoose, prop, getModelForClass,Ref } from '@typegoose/typegoose';
import { Genre } from './Genre';
import { User } from './User';

export class Movie{
    
    @prop({type:mongoose.Schema.Types.Number})
    popularity!: number;

    @prop({type:mongoose.Schema.Types.String})
    name!: string;

    @prop({type:mongoose.Schema.Types.String})
    director!:string;

    @prop({type:mongoose.Schema.Types.Number})
    imdb_score!:number;

    @prop({ref:User})
    createdBy!:Ref<User>;

    @prop({ref:Genre})
    genre!:Ref<Genre>[];

    @prop({type:mongoose.Schema.Types.Boolean,default:true})
    status!:boolean;

}

export const MovieModel = getModelForClass(Movie, { schemaOptions: { timestamps: true } });

