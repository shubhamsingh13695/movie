import { Request, Response } from "express";
import { GenreModel } from "../entity/Genre";

class GenreModule {

    async list(req: Request, res: Response) {
        try {
            let genre = await GenreModel.find({});
            return res.status(200).send({ msg: "Genre Fetched.", data: genre })
        }
        catch (ex) {
            console.log(ex);
        }

    }

    async create(req: Request, res: Response) {
        try {
            console.log(req);
            let name = req.body.name;
            await GenreModel.create({
                name: name
            });

            return res.status(200).send({ msg: "Genre Created." })
        }
        catch (ex) {
            console.log(ex);
            return res.status(500).send({ errors: "Something Went Wrong" })
        }
    }

}

export const genreModule = new GenreModule();