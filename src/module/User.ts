import { Request, Response } from "express";
import { UserModel } from "../entity/User";
import { compare, hash } from 'bcryptjs';
import { sign } from "jsonwebtoken";

class UserModule {

    async create(req: Request, res: Response) {
        try {
            let name: string = req.body.name;
            let email: string = req.body.email;
            let password: string = await hash(req.body.password, Math.random());

            let isUseExist = await UserModel.findOne({ email: email });

            if (isUseExist) {
                return res.status(422).send({ errors: "User already Registerd" });
            }

            await UserModel.create({ name: name, email: email, password: password });

            return res.status(200).send({ msg: "User Created." });
        }
        catch (ex) {
            return res.status(500).send({ errors: "Something Went Wrong" })
        }
    }


    async login(req: Request, res: Response) {
        try {
            let email: string = req.body.email;
            let password: string = req.body.password;

            let useExist = await UserModel.findOne({ email: email });

            if (useExist) {
                const valid = await compare(password, useExist.password);
                if (!valid) {
                    return res.status(401).send({ msg: "Invalid Email or Password" });
                }

                let access_token = sign({ userId: useExist._id }, process.env.TOKEN_SECRET!, {
                    expiresIn: process.env.ACCESS_TOKEN_EXPIRES_IN!
                })
                return res.status(200).send({ access_token: access_token, msg: "Login successfully." })

            }
            return res.status(401).send({ msg: "Invalid Email or Password" });
        }
        catch (ex) {
            console.log(ex)
            return res.status(500).send({ errors: "Something Went Wrong" })
        }
    }
}

export const userModule = new UserModule();

