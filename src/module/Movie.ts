import { Request, Response } from "express";
import { Movie, MovieModel } from "../entity/Movie";

class MovieModule {

    async create(req: Request, res: Response) {
        try {
            let name = req.body.name;
            let popularity = req.body.popularity;
            let director = req.body.director;
            let imdb_score = req.body.imdb_score;
            let createdBy = req.body.user._id;
            let genre = req.body.genre;

            await MovieModel.create({
                name: name,
                popularity: popularity,
                director: director,
                imdb_score: imdb_score,
                createdBy: createdBy,
                genre: genre
            })

            return res.status(200).send({ msg: "Movie Created successfully" });

        }
        catch (ex) {
            return res.status(500).send({ errors: "Something Went Wrong" });
        }
    }


    async update(req: Request, res: Response) {
        try {
            let id = req.params.id
            let popularity = req.body.popularity;
            let director = req.body.director;
            let imdb_score = req.body.imdb_score;
            let createdBy = req.body.user._id;
            let genre = req.body.genre;

            let isMovieExist = await MovieModel.countDocuments({ _id: id,status:true });
            if (!isMovieExist) {
                return res.status(422).send({ errors: "Movie does not exist" });
            }

            await MovieModel.findByIdAndUpdate(id, {
                popularity: popularity,
                director: director,
                imdb_score: imdb_score,
                createdBy: createdBy,
                genre: genre
            });
            return res.status(200).send({ msg: "Movie Created successfully" });
        }
        catch (ex) {
            return res.status(500).send({ errors: "Something Went Wrong" });
        }
    }


    async deleteMovie(req: Request, res: Response) {
        try {
            let id = req.params.id
            let isMovieExist = await MovieModel.countDocuments({ _id: id,status:true });
            if (!isMovieExist) {
                return res.status(422).send({ errors: "Movie does not exist" });
            }

            await MovieModel.findByIdAndUpdate(id, {
                status: false
            });
            return res.status(200).send({ msg: "Movie deleted" });
        }
        catch (ex) {
            return res.status(500).send({ errors: "Something Went Wrong" });
        }
    }

    async getMovie(req: Request, res: Response) {
        try {
            let id = req.params.id
            let isMovieExist = await MovieModel.countDocuments({ _id: id,status:true });
            if (!isMovieExist) {
                return res.status(422).send({ errors: "Movie does not exist" });
            }

            let movie = await MovieModel.findById(id, {
                status: false
            });
            return res.status(200).send({ msg: "Movie list fetched", data: movie });
        }
        catch (ex) {
            return res.status(500).send({ errors: "Something Went Wrong" });
        }
    }

    async list(req: Request, res: Response) {
        try {
            let limit = req.query.limit ? parseInt(<string>req.query.limit) : 18;
            let page = req.query.page ? parseInt(<string>req.query.page) : 1;
            let search = req.query.search;
            let orderBy = req.query.orderBy ? <string>req.query.orderBy : 'name';
            let order = req.query.order ? req.query.order : 1;
            let genre = req.query.genre ? <string>req.query.genre : "";

            let baseFilter: any = {
                $and: [
                    { status: true },
                ],
            };
            if (search && search!=="") {
                baseFilter["$or"] = [];
                //regex for movie name
                baseFilter.$or.push({ name: { '$regex': search, '$options': 'i' } });

                //regex for director name
                baseFilter.$or.push({ director: { '$regex': search, '$options': 'i' } });
            }
            if (genre && genre !== "") {
                //add all the genre
                baseFilter.$and.push({ genre: { $in: genre.split(",") } });
            }

            let sort: any = {};
            sort[orderBy] = order;
            let total = await MovieModel.countDocuments(baseFilter)
            let filterData = await MovieModel.find(baseFilter)
                .sort(sort)
                .skip((limit * page) - limit)
                .limit(limit)
                .populate(['genre']);


            let response: PaginatedList = {
                total: total,
                page: page,
                data: filterData,
                start: (limit * page) - limit + 1,
                end: (limit * page) - (limit - filterData.length),
                limit: limit,
                lastPage: Math.ceil(total / limit)
            }
            return res.status(200).send({ msg: "Movie list fetched", data: response });

        }
        catch (ex) {
            console.log(ex);
            return res.status(500).send({ errors: "Something Went Wrong" });
        }
    }

}

export const movieModule = new MovieModule();

interface PaginatedList {
    data: Movie[];
    total: number,
    page: number,
    start: number,
    end: number,
    limit: number,
    lastPage: number
}

