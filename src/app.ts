import "dotenv/config";
import express, { Application, Request, Response } from "express";
import "reflect-metadata";
import fileUpload from "express-fileupload";
import { router } from "./router/router";
import bodyParser from "body-parser";
import { connect } from "mongoose";
import cors from "cors";

async function bootstrap() {
    const app: Application = express();
    
    app.use(cors());
    
    app.get('/', (req: Request, res: Response) => {
        res.send("Server Started");
    });
    const mongoose = await connect(process.env.DB_CONN!, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    });

    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())

    app.use("/api", router);
    app.use(fileUpload());

    const host = '0.0.0.0';
    const port = parseInt(<string>process.env.PORT) || 3000;

    app.listen(port,host, () => {
        console.log(`server started ${host+`:`+port}`);
    }).on("error", (error) => {
        console.log(error);
    });

};

bootstrap();

